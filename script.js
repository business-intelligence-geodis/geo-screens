var ipcRenderer = require('electron').ipcRenderer
const { logger, currDayPath } = require('./logger')

const log = logger.createSimpleLogger(currDayPath+'/script.log')

const SLIDE = 'slide'
const CONTENT = 'CONTENT'
const splash = `<h1 style="position: fixed; z-index:1; padding-left:50px;">Downloading assets...</h1><img id="splash" class="slide-in-right" style="height: 100%" src="splash.png">`
const imgTemplate = '<img id="img" class="slide-in-right" style="height: 100%" src="CONTENT">'
const videoTemplate = '<video id="video" class="slide-in-right" style="height: 100%" src="CONTENT" autoplay>'
const iframeTemplate = '<iframe id="iframe" class="slide-in-right" src="CONTENT" style="width: 100%; height:100%;">'
// const tweetsTemplate = '<pre style="color: white">CONTENT</pre>'
const tweetsTemplate = `<div class="slide-in-right tweet-container"> <div class="header-tweets"> <img src="img/logo.png"> <img src="img/twitter.svg"> </div><div class="row"> TWEETS </div></div></div>`

const tweetTemplate = `<div class="col-md-6"> <div class="tweet"> <div class="tweet-header"> <div class="Tweet-brand u-floatRight"> <a href="#!" data-scribe="element:logo"> <span class="FollowButton-bird"> <div class="Icon Icon--twitter " aria-label="View on Twitter" title="View on Twitter" role="presentation"></div></span> </a> </div><div class="TweetAuthor js-inViewportScribingTarget js-aBScribingTarget"> <a target="_blank" class="TweetAuthor-link Identity u-linkBlend"> <span class="TweetAuthor-avatar Identity-avatar"> <img class="Avatar Avatar--edge" src="TWEET_USER_PROFILE_IMAGE"> </span> <span class="TweetAuthor-decoratedName"> <span class="TweetAuthor-name TweetAuthor-name--flex Identity-name customisable-highlight"> TWEET_USER_NAME </span> </span> <span class="TweetAuthor-screenName Identity-screenName">@TWEET_USER_SCREEN_NAME</span> </a> </div></div><div class="Tweet-body e-entry-content"> <p class="Tweet-text e-entry-title"> TWEET_TEXT </p><div class="Tweet-metadata dateline"> <a class="u-linkBlend u-url customisable-highlight long-permalink" href="#!"> <time> TWEET_CREATED_AT</time> </a> </div><ul class="Tweet-actions"> <li class="Tweet-action"> <a class="TweetAction TweetAction--heartEdge web-intent" href="#!"> <div class="Icon Icon--heart TweetAction-icon Icon--heartEdge"></div><span class="TweetAction-stat">TWEET_FAVORITE_COUNT</span> </a> </li><li class="Tweet-action"> <a class="TweetAction TweetAction--retweetEdge web-intent" href="#!"> <div class="Icon Icon--retweet TweetAction-icon Icon--retweetEdge"></div><span class="TweetAction-stat">TWEET_RETWEET_COUNT</span> </a> </li></ul> </div></div></div>`

const resources = require('./localResources.json')

const slideTime = 30000
const tweetsTime = 30000
const iframeTime = 70000
// const slideTime = 5000
// const tweetsTime = 5000
// const iframeTime = 5000
const IMAGE_TYPE = '1'
const VIDEO_TYPE = '2'
const TWEET_TYPE = '8'
const IFRAME_TYPE = '4'

var isSplash = true

const TWEETS = 'TWEETS'
const TWEET_TEXT = 'TWEET_TEXT'
const TWEET_USER_NAME = 'TWEET_USER_NAME'
const TWEET_USER_SCREEN_NAME = 'TWEET_USER_SCREEN_NAME'
const TWEET_USER_PROFILE_IMAGE = 'TWEET_USER_PROFILE_IMAGE'
const TWEET_CREATED_AT = 'TWEET_CREATED_AT'
const TWEET_RETWEET_COUNT = 'TWEET_RETWEET_COUNT'
const TWEET_FAVORITE_COUNT = 'TWEET_FAVORITE_COUNT'

var currentIndex = 0

var sliderController = {
  timer: null,
  slides: resources.data,
  currentIndex,
  play () {
    if (!this.slides.length) {
      return
    }
    this.currentIndex = 0
    this.timer = setTimeout(() => {
      this.displaySlide(true)
    }, this.getSlideTime())
  },

  getSlideHTML () {
    switch (this.slides[this.currentIndex].type) {
      case IMAGE_TYPE:
        return this.getImageHTML()
      case VIDEO_TYPE:
        return this.getVideoHTML()
      case TWEET_TYPE:
        return this.getTweetHTML()
      case IFRAME_TYPE:
        return this.getIFrameHTML()
    }
  },

  getImageHTML () {
    return imgTemplate.replace(CONTENT, this.slides[this.currentIndex].src)
  },

  getVideoHTML () {
    return videoTemplate.replace(CONTENT, this.slides[this.currentIndex].src)
  },

  getTweetHTML () {
    // return tweetsTemplate.replace(CONTENT, JSON.stringify(this.slides[this.currentIndex].tweets, null, 2))
    let tweetsHTML = ''
    if (this.slides[this.currentIndex].tweets) {
      this.slides[this.currentIndex].tweets.forEach(t => {
        let tweet = tweetTemplate
        tweet = tweet.replace(TWEET_TEXT, t.text)
        tweet = tweet.replace(TWEET_USER_NAME, t.user.name)
        tweet = tweet.replace(TWEET_USER_SCREEN_NAME, t.user.screen_name)
        tweet = tweet.replace(TWEET_USER_PROFILE_IMAGE, t.user.profile_image_url_https)
        tweet = tweet.replace(TWEET_CREATED_AT, t.created_at)
        tweet = tweet.replace(TWEET_RETWEET_COUNT, t.retweet_count)
        tweet = tweet.replace(TWEET_FAVORITE_COUNT, t.favorite_count)
        tweetsHTML += tweet
      })
    }
    console.log(tweetsTemplate.replace(TWEETS, tweetsHTML)); log.info(tweetsTemplate.replace(TWEETS, tweetsHTML))
    return tweetsTemplate.replace(TWEETS, tweetsHTML)
  },

  getIFrameHTML () {
    return iframeTemplate.replace(CONTENT, this.slides[this.currentIndex].src)
  },

  getSlideTime () {
    if ((this.slides.length) === this.currentIndex) {
      return 1000
    }
    if(isSplash){
      console.log("isSplash: ",isSplash); log.info("isSplash: ",isSplash)
      isSplash=false
      return 1000
    }
    switch (this.slides[this.currentIndex].type) {
      
      case IMAGE_TYPE:
        return slideTime
      case VIDEO_TYPE:
        //return 10000
	      return this.slides[this.currentIndex].duration * 1000
      case TWEET_TYPE:
        return !this.slides[this.currentIndex].tweets ? 100 : tweetsTime
      case IFRAME_TYPE:
        return iframeTime
    }
  },

  displaySlide (iterate = false) {
    if (iterate) {
      this.timer = setTimeout(() => {
        if ((this.slides.length - 1) === this.currentIndex) {
          console.log('DONE PLAYING!'); log.info('DONE PLAYING!')
          console.log('Reloading'); log.info('Reloading')
          clearTimeout(this.timer)
          window.location.reload()
          return
        }
        this.currentIndex++
        this.displaySlide(true)
      }, this.getSlideTime())
      // }, this.currentIndex === 0 ? 1000 : this.getSlideTime())
    }
    console.log(`playing: ${this.currentIndex}`, this.slides[this.currentIndex]); log.info(`playing: ${this.currentIndex}`, this.slides[this.currentIndex])
    document.getElementById(SLIDE).innerHTML = this.getSlideHTML()
  }
}

/**
 * splash screen
 */
setTimeout(() => {
  document.getElementById(SLIDE).innerHTML = splash
  console.log('splash'); log.info('splash')
  console.log('emiting: fetch'); log.info('emiting: fetch')
  ipcRenderer.send('fetch', false)
}, 100)

// on downloaded assets
ipcRenderer.on('downloaded-assets', (event, arg) => {
  console.log('main says: fetched!'); log.info('main says: fetched!')
  console.log(`Changes? ${arg}`); log.info(`Changes? ${arg}`)

  if (arg) {
    console.log(`reloading!!`); log.info(`reloading!!`)
    window.location.reload()
  }

  sliderController.timer = setTimeout(() => {
    console.log('play!'); log.info('play!')
    sliderController.play()
  }, 1000)
})

/**
 * On error loading image or video, force download all assets
 * force=true
 */
window.addEventListener('error', function (e) {
  console.log(`error loading: ${sliderController.currentIndex}`, e); log.info(`error loading: ${sliderController.currentIndex}`, e)
  console.log(sliderController.slides[sliderController.currentIndex]); log.info(sliderController.slides[sliderController.currentIndex])
  if (e.target.id === 'img' || e.target.id === 'video') {
    clearTimeout(sliderController.timer)
    ipcRenderer.send('fetch', true)
    document.getElementById(SLIDE).innerHTML = splash
  }
}, true)
