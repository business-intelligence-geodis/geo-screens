const axios = require('axios')
const fs = require('fs')
const path = require('path')
var _ = require('lodash')
const { logger, currDayPath } = require('./logger')

const log = logger.createSimpleLogger(currDayPath+'/fetcher.log')

const localResourcesFileName = 'localResources.json'
const resourcesFileName = 'resources.json'


var resources = null
var newResources = null

const filesPath = [__dirname, 'display_files']

const fetch = async function (url, force=false) {
  resources = JSON.parse(fs.readFileSync(__dirname+'/'+resourcesFileName, 'utf8'))
  console.log('resources.data.length: ',resources.data.length); log.info('resources.data.length: ',resources.data.length)
  console.log(`Downloading resources... force: ${force}`); log.info(`Downloading resources... force: ${force}`)
  let changes = false

  let noTweets = _.reduce(resources.data, filterTweets, [])

  if (url) {
    console.log(`url to get json: ${url}`); log.info(`url to get json: ${url}`)
    await axios.get(url).then(response => {
      newResources = response.data
      let newNoTweets = _.reduce(newResources.data, filterTweets, [])
      changes = getResourcesChanges(noTweets, newNoTweets)
      console.log(`total assets: ${getTotalAssets()}`); log.info(`total assets: ${getTotalAssets()}`)
    }).catch(err => {
      console.log('ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', err); log.info('ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', err)
      writeLogFile(err)
    })
    
    if (newResources) {
      if (force) {
        deleteFiles()
      }
      changes = downloadFiles(changes,force)
      fs.writeFileSync(__dirname+'/'+localResourcesFileName, JSON.stringify(newResources), 'utf-8')
    }
  }
  else{
    console.log('URL does not exist'); log.info('URL does not exist')
  }
  return changes
}

const isAsset = function (type) {
  switch (type) {
    case '1':
    case '2':
      return true
  }
  return false
}

const filterTweets = function (result, value, key) {
  return value.type === 8 || value.type === '8' ? result : result.concat(value)
}

const getResourcesChanges = function(noTweets, newNoTweets){
  if (!_.isEqual(noTweets, newNoTweets)){
    console.log('Changes in resources!!'); log.info('Changes in resources!!')
    console.log('differences: ',_.differenceWith(noTweets, newNoTweets,_.isEqual)); log.info('differences: ',_.differenceWith(noTweets, newNoTweets,_.isEqual))
    fs.writeFileSync(__dirname+'/'+resourcesFileName, JSON.stringify(newResources), 'utf-8')
    return true
  }
  console.log('No changes in resources'); log.info('No changes in resources')
  return false
}

const deleteFiles = function(){
  let dir = fs.readdirSync(path.join(...filesPath))
  console.log('deleting files'); log.info('deleting files')
  dir.forEach(e => {
    let toDelete = path.join(...filesPath, e)
    fs.unlinkSync(toDelete)
    console.log(`deleted ${toDelete}`); log.info(`deleted ${toDelete}`)
  })
}

const downloadFiles = async function(changes,force){
  let _changes = changes;

  for (let i = 0; i < newResources.data.length; i++) {
    if (isAsset(newResources.data[i].type)) {
      let fileUrl = newResources.data[i].src
      let fileName = newResources.data[i].src.split('/')[newResources.data[i].src.split('/').length - 1]
      let localFile = path.join(...filesPath, fileName)

      newResources.data[i].src = localFile
      if (force || (!fs.existsSync(localFile) || (fs.existsSync(localFile) && fs.statSync(localFile).size === 0))) {
        console.log(`downloading: ${localFile}`); log.info(`downloading: ${localFile}`)
        await axios.get(fileUrl, {responseType: 'stream'}).then((response) => {
          response.data.pipe(fs.createWriteStream(localFile))
          console.log(`downloaded: ${localFile}`); log.info(`downloaded: ${localFile}`)
          _changes = true
        })
      } else {
        console.log(`file already exists: ${localFile}`); log.info(`file already exists: ${localFile}`)
      }
    }
  }
  return _changes;
}

const writeLogFile = function(msg){
  let timestamp = new Date().getTime()
  fs.writeFileSync(__dirname+'/logs/log_${timestamp}.json', JSON.stringify(msg, null, 2), 'utf-8')
}

const getTotalAssets = function(){
  return newResources.data.filter(e => isAsset(e.type)).length
}


module.exports = {
  fetch
}
