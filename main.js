const { app } = require('electron')
const { BrowserWindow } = require('electron')
const { fetch } = require('./fetcher')
const config = require('./config.json')
const url = config.url

const { logger, currDayPath } = require('./logger')
const log = logger.createSimpleLogger(currDayPath+'/main.log')

var downloading = false

function createWindow () {
  let win = new BrowserWindow({
    width: 1600,
    height: 1000,
    webPreferences: {
      nodeIntegration: true
    }
  })
  // open dev tools
  //win.webContents.openDevTools()
  // and load the index.html of the app.
  win.loadFile('index.html')
  win.setFullScreen(true)
  win.setMenu(null)
  // Create the browser window.
}

app.on('ready', createWindow)

// In main process.
const { ipcMain } = require('electron')

// Fetch
ipcMain.on('fetch', (event, arg) => {
  console.log('fetch force: ', arg); log.info('fetch force: ', arg)
   if (!downloading) {
    downloading = true
    fetch(url, arg).then((e) => {
      console.log('fetch promise: ', e); log.info('fetch promise: ', e)
      console.log('emiting: downloaded-assets'); log.info('emiting: downloaded-assets')
      event.reply('downloaded-assets', e)
      downloading = false
    })
  } else {
    console.log('already downloading waiting download'); log.info('already downloading waiting download')
  }
  event.returnValue = 'ok' 
})
