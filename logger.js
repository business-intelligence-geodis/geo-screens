const logger = require('simple-node-logger')
const fs = require('fs')
let currDayPath = __dirname+'/'+'project_logs/'+getTimeDate()

//Creating current day folder
if (!fs.existsSync(currDayPath)) {
    fs.mkdir(currDayPath,{},(err) => {
        if (err) throw err;
    });
}

//Returns YYYY-MM-DD
function getTimeDate(){ 
    let dateObj = new Date;
    let curr_date = dateObj.getDate()
    let curr_month = dateObj.getMonth()
    curr_month = curr_month + 1
    let curr_year = dateObj.getFullYear()
    let curr_min = dateObj.getMinutes()
    let curr_hr= dateObj.getHours()
    let curr_sc= dateObj.getSeconds()

    if(curr_month.toString().length == 1)
        curr_month = '0' + curr_month
    if(curr_date.toString().length == 1)
        curr_date = '0' + curr_date
    if(curr_hr.toString().length == 1)
        curr_hr = '0' + curr_hr
    if(curr_min.toString().length == 1)
        curr_min = '0' + curr_min

    return curr_year + "-" + curr_month + "-" + curr_date 
}

module.exports = {
    logger,
    currDayPath
}